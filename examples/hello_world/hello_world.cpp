/*
 * Copyright 2019 Richard Kjerstadius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * \file hello_world.cpp
 * \brief libus2066 'Hello World!' example
 */

#include "us2066.h"

#include "Arduino.h"

/* Display pin definitions */
constexpr uint8_t cs    = 13;
constexpr uint8_t d4    = 7;
constexpr uint8_t d5    = 10;
constexpr uint8_t d6    = 8;
constexpr uint8_t d7    = 9;
constexpr uint8_t dc    = 12;
constexpr uint8_t en    = 11;
constexpr uint8_t res   = 5;
constexpr uint8_t rw    = 6;
constexpr uint8_t vccEn = 21;
constexpr uint8_t vddEn = 22;

constexpr uint8_t displayNumCols  = 20;
constexpr uint8_t displayNumLines = 4;

int main()
{
    pinMode(vddEn, OUTPUT);
    pinMode(vccEn, OUTPUT);
    digitalWriteFast(vddEn, HIGH);

    us2066::Parallel68xx interface(cs, dc, en, rw, d4, d5, d6, d7);
    us2066::Us2066<us2066::Parallel68xx> oled(interface, res);

    oled.begin(displayNumCols, displayNumLines);
    digitalWriteFast(vccEn, HIGH);
    delay(100);
    oled.display();
    delay(200);

    oled.println("Hello World!");
    while (true) {
        oled.scrollDisplayRight();
        delay(250);
    }

    return 0;
}
