Contributing
============

Contributions are very welcome. After all, I want this library to be useful not
only for myself, but for others as well. If you have an improvement, feature
addition or a bug fix that you think should be mainlined, please submit a
[merge request].

I have a strong preference for commit messages that follow
[these rules][7 rules], so please take that into consideration when creating
your merge request.

Please make sure that your merge request successfully completes the CI pipeline.
Merge requests that do not pass all required CI steps will not be considered.

Code Style
----------

This project uses a `.clang-format` file to define the expected code style.
Please ensure that you run `clang-format` on any new or modified code before
creating a merge request. The build *will* fail if the style isn't correct.

While there are many ways to run `clang-format`, the most straightforward in the
context of this project is likely to be the target provided by Meson. To invoke
it, run `ninja clang-format` from your build directory. Other options include
using [`git clang-format`][git-clang-format] or running `clang-format` directly
on each relevant file.

[7 rules]: https://chris.beams.io/posts/git-commit/#seven-rules
[git-clang-format]: https://github.com/llvm-mirror/clang/blob/master/tools/clang-format/git-clang-format
[merge request]: https://gitlab.com/kjerstadius/libus2066-arduino/merge_requests
