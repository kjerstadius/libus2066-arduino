/*
 * Copyright 2019 Richard Kjerstadius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * \file us2066_Settings.h
 * \brief us2066 initialization settings definitions
 */

#pragma once

#include "us2066_Enums.h"
#include <cstdint>

namespace us2066 {

enum GpioSettings : uint8_t {
    inputDisabled = 0x00, //!< Pin in Hi-Z. Will always be read as low.
    inputEnabled  = 0x01, //!< Pin in Hi-Z with input enabled.
    outputLow     = 0x02, //!< (POR) Pin in output mode, set low.
    outputHigh    = 0x03  //!< Pin in output mode, set high.
};

enum IoLevel : uint8_t {
    lowVoltage = 0x00, //!< Disables the internal VDD regulator for 2.8V I/O.
    TTL        = 0x5c  //!< (POR) Enables the internal VDD regulator for 5V I/O.
};

enum VcomDeselect : uint8_t {
    level1 = 0x00, //!< 0.65 x Vcc
    level2 = 0x01, //!< 0.71 x Vcc
    level3 = 0x02, //!< (POR) 0.77 x Vcc
    level4 = 0x03, //!< 0.83 x Vcc
    level5 = 0x04  //!< 1.0 x Vcc
};

/*! VSL is the reference pin for the segment low level voltage. It can be either
 * internal or external depending on application and should be set
 * appropriately.
 */
enum VslSelect : uint8_t { internalVsl = 0x0, externalVsl = 0x1 };

struct DefaultSettings {
    /*! Defines the startup contrast level. Valid settings are 0-255.
     * POR value is 0x7f. */
    const static uint8_t contrast = 0U;

    /*! Controls the startup cursor direction. Controls the same setting as
     * Us2066::leftToRight() and Us2066::rightToLeft(). POR is equivalent to
     * leftToRight. */
    const static uint8_t cursorDirection =
        static_cast<uint8_t>(Bitmasks::leftToRight);

    /*! Configure US2066 GPIO pin. See GpioSettings. */
    const static uint8_t gpioSetting = GpioSettings::inputDisabled;

    /*! Configure IO voltage level. US2066 can be configured for either 2.8V or
     * 5V IO. Also see IoLevel. */
    const static uint8_t ioLevel = IoLevel::lowVoltage;

    /*! Controls the divide ratio for the display clock (DCLK) according to the
     * formula \$f divide ratio = clockDivideRatio + 1\$f. Valid values are
     * 0-15 and POR value is 0. */
    const static uint8_t clockDivideRatio = 0U;
    /*! Set display oscillator frequency. Valid values are 0-15 with the
     * frequency increasing as the value goes up. Note that the datasheet
     * doesn't clearly specify exactly what the frequency ends up being.
     * POR value is 7. */
    const static uint8_t oscillatorFrequency = 7U; // POR

    /*! Set the data shift direction of common. Can be "normal order", i.e. 0 to
     * 31, or "reverse order", i.e. 31 to 0. Setting to reverse basically means
     * that text appear as if mirrored. POR setting is equivalent to reverse
     * order. */
    // FIXME: Should this be an API command instead? There should be one for
    // segDirection too.
    const static uint8_t comDirection =
        static_cast<uint8_t>(Bitmasks::com0To31);

    /*! Set phase 1 length of segment waveform driver. The unit is DCLK cycles
     * and valid values are 0-15, resulting in a period of 2-32 cycles. POR
     * value is 8, i.e. 16 cycles. */
    const static uint8_t phase1Length = 3U;
    /*! Set phase 2 length of segment waveform driver. The unit is DCLK cycles
     * and valid values are 0-15, resulting in a period of 1-15 cycles. POR
     * value is 7, which results in 8 cycles. */
    const static uint8_t phase2Length = 7U;

    /*! Configures the mapping between display data column addresses and the
     * segment driver. It seems this is mainly to allow a flexible OLED module
     * design. Depending on the exact OLED display manufacturer and model this
     * might need tweaking. Note that changing this setting won't apply to data
     * already in DDRAM. Each setting can be either on or off, i.e. 0 or 1. */
    const static uint8_t segPinLeftRightRemap = 0U;
    const static uint8_t segPinSequential     = 1U;

    /*! Adjust the Vcom regulator output, which affects the display brightness.
     * See VcomDeselect. */
    const static uint8_t vcomDeselectLevel = VcomDeselect::level4;

    /*! Select internal or external VSL (reference pin). See VslSelect. */
    const static uint8_t vslSelect = VslSelect::externalVsl;
};

} // namespace us2066
