/*
 * Copyright 2019 Richard Kjerstadius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * \file us2066_Enums.cpp
 * \brief Operator implementations for us2066 enum classes
 */

#include "us2066_Enums.h"

#include <type_traits>

namespace us2066 {

uint8_t operator|(FundamentalCommands lhs, uint8_t rhs)
{
    return static_cast<uint8_t>(
        static_cast<std::underlying_type<FundamentalCommands>::type>(lhs)
        | static_cast<uint8_t>(rhs));
}

uint8_t operator&(FundamentalCommands lhs, uint8_t rhs)
{
    return static_cast<uint8_t>(
        static_cast<std::underlying_type<FundamentalCommands>::type>(lhs)
        & static_cast<uint8_t>(rhs));
}

uint8_t operator|(ExtendedCommandsRE lhs, uint8_t rhs)
{
    return static_cast<uint8_t>(
        static_cast<std::underlying_type<ExtendedCommandsRE>::type>(lhs)
        | static_cast<uint8_t>(rhs));
}

uint8_t operator&(ExtendedCommandsRE lhs, uint8_t rhs)
{
    return static_cast<uint8_t>(
        static_cast<std::underlying_type<ExtendedCommandsRE>::type>(lhs)
        & static_cast<uint8_t>(rhs));
}

uint8_t operator|(ExtendedCommandsIS lhs, uint8_t rhs)
{
    return static_cast<uint8_t>(
        static_cast<std::underlying_type<ExtendedCommandsIS>::type>(lhs)
        | static_cast<uint8_t>(rhs));
}

uint8_t operator&(ExtendedCommandsIS lhs, uint8_t rhs)
{
    return static_cast<uint8_t>(
        static_cast<std::underlying_type<ExtendedCommandsIS>::type>(lhs)
        & static_cast<uint8_t>(rhs));
}

uint8_t operator|(OledCommands lhs, uint8_t rhs)
{
    return static_cast<uint8_t>(
        static_cast<std::underlying_type<OledCommands>::type>(lhs)
        | static_cast<uint8_t>(rhs));
}

uint8_t operator&(OledCommands lhs, uint8_t rhs)
{
    return static_cast<uint8_t>(
        static_cast<std::underlying_type<OledCommands>::type>(lhs)
        & static_cast<uint8_t>(rhs));
}

uint8_t operator|(Bitmasks lhs, uint8_t rhs)
{
    return static_cast<uint8_t>(
        static_cast<std::underlying_type<Bitmasks>::type>(lhs)
        | static_cast<uint8_t>(rhs));
}

uint8_t operator&(Bitmasks lhs, uint8_t rhs)
{
    return static_cast<uint8_t>(
        static_cast<std::underlying_type<Bitmasks>::type>(lhs)
        & static_cast<uint8_t>(rhs));
}

Bitmasks operator~(Bitmasks lhs)
{
    return static_cast<Bitmasks>(
        ~static_cast<std::underlying_type<Bitmasks>::type>(lhs));
}

} // namespace us2066
