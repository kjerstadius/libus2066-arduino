/*
 * Copyright 2019 Richard Kjerstadius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * \file us2066.h
 * \brief Us2066 class declaration
 */

#pragma once

#include "us2066_Enums.h"
#include "us2066_Interface.h"
#include "us2066_Settings.h"

#include "Arduino.h"
#include <cstdint>

namespace us2066 {

template<typename Interface, typename Settings = DefaultSettings>
class Us2066 : public Print {
  public:
    Us2066(Interface &interface, uint8_t resetBar);
    // FIXME: Destructor?

    void begin(uint8_t columns, uint8_t rows, uint8_t wideChar = 0);

    void autoscroll();
    void noAutoscroll();
    void blink();
    void noBlink();
    void clear();
    void cursor();
    void noCursor();
    void display();
    void noDisplay();
    void home();
    void leftToRight();
    void rightToLeft();
    void scrollDisplayLeft();
    void scrollDisplayRight();
    void setCursor(uint8_t column, uint8_t row);

    void doubleHeight();
    void noDoubleHeight();
    void doubleHeightMode(DoubleHeightMode mode);

    void enableExtensionIS();
    void disableExtensionIS();
    void enableExtensionRE();
    void disableExtensionRE();
    void enableOledCharacterization();
    void disableOledCharacterization();

    void command(FundamentalCommands fundamentalCommand);
    void command(ExtendedCommandsIS extendedCommand);
    void command(ExtendedCommandsRE extendedCommand);
    void command(OledCommands oledCommand);
    void command(uint8_t command);
    void data(uint8_t byte);
    size_t write(uint8_t byte);

  private:
    Interface &mInterface;
    uint8_t mResetBar;

    uint8_t mColumns;
    uint8_t mRows;

    uint8_t mCursorDisplayShiftSettings   = 0x0;
    uint8_t mDisplayControlSettings       = 0x0;
    uint8_t mDoubleHeightSettings         = 0x0;
    uint8_t mEntryModeSettings            = Settings::cursorDirection;
    uint8_t mExtendedEntryModeSettings    = Settings::comDirection;
    uint8_t mExtendedFunctionSetSettings  = 0x0;
    uint8_t mFunctionSetSettings          = 0x0;
    uint8_t mOledCharacterizationSettings = 0x0;
};

} // namespace us2066

#include "us2066.hpp"
