/*
 * Copyright 2019 Richard Kjerstadius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * \file us2066_Interface.cpp
 * \brief us2066 Interface class implementations
 */

#include "us2066_Interface.h"

#include "Arduino.h"

namespace us2066 {

Parallel68xx::Parallel68xx(uint8_t csBar, uint8_t dc, uint8_t enable,
                           uint8_t rw, uint8_t d4, uint8_t d5, uint8_t d6,
                           uint8_t d7)
    : mCsBar(csBar), mDc(dc), mEnable(enable), mRw(rw), mD4(d4), mD5(d5),
      mD6(d6), mD7(d7)
{
    pinMode(mCsBar, OUTPUT);
    pinMode(mDc, OUTPUT);
    pinMode(mEnable, OUTPUT);
    pinMode(mRw, OUTPUT);

    digitalWriteFast(mCsBar, HIGH);
    digitalWriteFast(mDc, HIGH);
    digitalWriteFast(mEnable, LOW);
    digitalWriteFast(mRw, LOW);

    digitalWriteFast(mD4, LOW);
    digitalWriteFast(mD5, LOW);
    digitalWriteFast(mD6, LOW);
    digitalWriteFast(mD7, LOW);
}

size_t Parallel68xx::transmit(uint8_t byte, TransmitMode mode)
{
    pinMode(mD4, OUTPUT);
    pinMode(mD5, OUTPUT);
    pinMode(mD6, OUTPUT);
    pinMode(mD7, OUTPUT);

    digitalWriteFast(mDc, static_cast<uint8_t>(mode));
    // Write is implicit when calling this function
    digitalWriteFast(mRw, LOW);
    digitalWriteFast(mCsBar, LOW);

    digitalWriteFast(mEnable, HIGH);
    digitalWriteFast(mD4, byte & 0x10);
    digitalWriteFast(mD5, byte & 0x20);
    digitalWriteFast(mD6, byte & 0x40);
    digitalWriteFast(mD7, byte & 0x80);
    digitalWriteFast(mEnable, LOW);

    /* The minimum cycle time between two consective enable pulses is 400 ns.
     * Since Teensy/Arduino doesn't provide a standardized delay of
     * less than 1 us, settle for 1 us. */
    // FIXME: Perhaps its possible to run without this delay?
    delayMicroseconds(1);

    digitalWriteFast(mEnable, HIGH);
    digitalWriteFast(mD4, byte & 0x01);
    digitalWriteFast(mD5, byte & 0x02);
    digitalWriteFast(mD6, byte & 0x04);
    digitalWriteFast(mD7, byte & 0x08);
    digitalWriteFast(mEnable, LOW);

    digitalWriteFast(mCsBar, HIGH);

    return 1;
}

} // namespace us2066
