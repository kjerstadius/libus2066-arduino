/*
 * Copyright 2019 Richard Kjerstadius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * \file us2066_Enums.h
 * \brief us2066 enum declarations
 */

#pragma once

#include <cstdint>

namespace us2066 {

enum Constants : uint8_t { maxRows = 4, maxColumns = 20, rowOffset = 0x20 };

enum class FundamentalCommands : uint8_t {
    clearDisplay    = 0x01,
    returnHome      = 0x02,
    entryModeSet    = 0x4,
    displayOnOff    = 0x8,
    shiftControl    = 0x10,
    functionSet     = 0x20,
    setCgramAddress = 0x40,
    setDdramAddress = 0x80
};

enum class ExtendedCommandsRE : uint8_t {
    extendedEntryModeSet         = 0x04,
    extendedFunctionSet          = 0x08,
    doubleHeightModeDisplayShift = 0x10,
    functionSet                  = 0x20,
    functionSelectA              = 0x71,
    functionSelectB              = 0x72,
    oledCharacterization         = 0x78,
    ScrollQuantity               = 0x80
};

enum class ExtendedCommandsIS : uint8_t { shiftScrollEnable = 0x10 };

enum class OledCommands : uint8_t {
    fadeOutBlinkMode           = 0x23,
    contrastControl            = 0x81,
    displayClockDividerOscFreq = 0xd5,
    phaseLength                = 0xd9,
    segPinsHwConfig            = 0xda,
    vcomhDeselectLevel         = 0xdb,
    functionSelectC            = 0xdc
};

enum class Bitmasks : uint8_t {
    // Cursor and display shift register
    shiftRight   = 0x04,
    displayShift = 0x08,

    // Display control settings register
    blinkOn   = 0x01,
    cursorOn  = 0x02,
    displayOn = 0x04,

    // Double height control and display/dot shift enable
    displayShiftEnable = 0x01,
    doubleHeightMode   = 0x0c,

    // Entry mode settings register
    autoscroll  = 0x01,
    leftToRight = 0x02,

    // Extended entry mode settings register
    seg0To99 = 0x01,
    com0To31 = 0x02,

    // Extended function set register
    threeFourLineDisplay = 0x01,
    invertCursorColor    = 0x02,
    sixDotFontWidth      = 0x04,

    // Function set register
    enableExtensionIS = 0x01,
    enableExtensionRE = 0x02,
    doubleHeight      = 0x04,
    numRowsEven       = 0x08,

    // OLED characterization register
    enableOledCharacterization = 0x01
};

enum class DoubleHeightMode : uint8_t {
    bottom = 0x00,
    entire = 0x02,
    middle = 0x01,
    top    = 0x03
};

enum class CharacterROMs : uint8_t {
    // Note: These names aren't necessarily great, so please refer to the
    // datasheet for specifications of what's in the different ROMs
    englishGreek        = 0x00,
    international       = 0x04,
    englishGreekSymbols = 0x08
};

enum class CGRAMStartAddress : uint8_t {
    addr240 = 0x00,
    addr248 = 0x01,
    addr250 = 0x02,
    addr256 = 0x03
};

// Non-templated operator overloads
uint8_t operator|(FundamentalCommands lhs, uint8_t rhs);
uint8_t operator&(FundamentalCommands lhs, uint8_t rhs);
uint8_t operator|(ExtendedCommandsRE lhs, uint8_t rhs);
uint8_t operator&(ExtendedCommandsRE lhs, uint8_t rhs);
uint8_t operator|(ExtendedCommandsIS lhs, uint8_t rhs);
uint8_t operator&(ExtendedCommandsIS lhs, uint8_t rhs);
uint8_t operator|(OledCommands lhs, uint8_t rhs);
uint8_t operator&(OledCommands lhs, uint8_t rhs);
uint8_t operator|(Bitmasks lhs, uint8_t rhs);
uint8_t operator&(Bitmasks lhs, uint8_t rhs);
Bitmasks operator~(Bitmasks lhs);

} // namespace us2066

#include "us2066_Enums.hpp"
