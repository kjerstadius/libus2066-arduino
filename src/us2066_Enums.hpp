/*
 * Copyright 2019 Richard Kjerstadius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * \file us2066_Enums.hpp
 * \brief Template definitions for us2066 enum classes
 */

#pragma once

#include <type_traits>

namespace us2066 {

template<typename T>
FundamentalCommands operator&(FundamentalCommands lhs, T rhs)
{
    return static_cast<FundamentalCommands>(
        static_cast<std::underlying_type<FundamentalCommands>::type>(lhs)
        & static_cast<typename std::underlying_type<T>::type>(rhs));
}

template<typename T>
FundamentalCommands operator|(FundamentalCommands lhs, T rhs)
{
    return static_cast<FundamentalCommands>(
        static_cast<std::underlying_type<FundamentalCommands>::type>(lhs)
        | static_cast<typename std::underlying_type<T>::type>(rhs));
}

template<typename T> ExtendedCommandsRE operator&(ExtendedCommandsRE lhs, T rhs)
{
    return static_cast<ExtendedCommandsRE>(
        static_cast<std::underlying_type<ExtendedCommandsRE>::type>(lhs)
        & static_cast<typename std::underlying_type<T>::type>(rhs));
}

template<typename T> ExtendedCommandsRE operator|(ExtendedCommandsRE lhs, T rhs)
{
    return static_cast<ExtendedCommandsRE>(
        static_cast<std::underlying_type<ExtendedCommandsRE>::type>(lhs)
        | static_cast<typename std::underlying_type<T>::type>(rhs));
}

template<typename T> ExtendedCommandsIS operator&(ExtendedCommandsIS lhs, T rhs)
{
    return static_cast<ExtendedCommandsIS>(
        static_cast<std::underlying_type<ExtendedCommandsIS>::type>(lhs)
        & static_cast<typename std::underlying_type<T>::type>(rhs));
}

template<typename T> ExtendedCommandsIS operator|(ExtendedCommandsIS lhs, T rhs)
{
    return static_cast<ExtendedCommandsIS>(
        static_cast<std::underlying_type<ExtendedCommandsIS>::type>(lhs)
        | static_cast<typename std::underlying_type<T>::type>(rhs));
}

template<typename T> OledCommands operator&(OledCommands lhs, T rhs)
{
    return static_cast<OledCommands>(
        static_cast<std::underlying_type<OledCommands>::type>(lhs)
        & static_cast<typename std::underlying_type<T>::type>(rhs));
}

template<typename T> OledCommands operator|(OledCommands lhs, T rhs)
{
    return static_cast<OledCommands>(
        static_cast<std::underlying_type<OledCommands>::type>(lhs)
        | static_cast<typename std::underlying_type<T>::type>(rhs));
}

template<typename T> Bitmasks operator&(Bitmasks lhs, T rhs)
{
    return static_cast<Bitmasks>(
        static_cast<std::underlying_type<Bitmasks>::type>(lhs)
        & static_cast<typename std::underlying_type<T>::type>(rhs));
}

template<typename T> Bitmasks operator|(Bitmasks lhs, T rhs)
{
    return static_cast<Bitmasks>(
        static_cast<std::underlying_type<Bitmasks>::type>(lhs)
        | static_cast<typename std::underlying_type<T>::type>(rhs));
}

template<typename T> uint8_t &operator&=(uint8_t &lhs, const T &rhs)
{
    lhs = static_cast<uint8_t>(
        lhs & static_cast<typename std::underlying_type<T>::type>(rhs));
    return lhs;
}

template<typename T> uint8_t &operator|=(uint8_t &lhs, const T &rhs)
{
    lhs = static_cast<uint8_t>(
        lhs | static_cast<typename std::underlying_type<T>::type>(rhs));
    return lhs;
}

} // namespace us2066
