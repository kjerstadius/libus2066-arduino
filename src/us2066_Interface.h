/*
 * Copyright 2019 Richard Kjerstadius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * \file us2066_Interface.h
 * \brief Declarations of us2066 Interface base class and derivatives
 */

#pragma once

#include <cstddef>
#include <cstdint>

namespace us2066 {

enum class TransmitMode : uint8_t { command = 0, data = 1 };

class Interface {
  public:
    virtual ~Interface(){};
    virtual size_t transmit(uint8_t byte, TransmitMode mode);
};

class Parallel68xx : public Interface {
  public:
    Parallel68xx(uint8_t csBar, uint8_t dc, uint8_t enable, uint8_t rw,
                 uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7);
    ~Parallel68xx(){};

    size_t transmit(uint8_t byte, TransmitMode mode);

  private:
    const uint8_t mCsBar;
    const uint8_t mDc;
    const uint8_t mEnable;
    const uint8_t mRw;

    const uint8_t mD4;
    const uint8_t mD5;
    const uint8_t mD6;
    const uint8_t mD7;
};

} // namespace us2066
