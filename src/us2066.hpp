/*
 * Copyright 2019 Richard Kjerstadius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * \file us2066.hpp
 * \brief Us2066 template definitions
 */

#pragma once

namespace us2066 {

template<typename Interface, typename Settings>
Us2066<Interface, Settings>::Us2066(Interface &interface, uint8_t resetBar)
    : mInterface(interface), mResetBar(resetBar)
{
    pinMode(mResetBar, OUTPUT);

    // Reset pin cycle delay times taken from US2066 datasheet (p.16)
    digitalWriteFast(mResetBar, LOW);
    delayMicroseconds(3);
    digitalWriteFast(mResetBar, HIGH);
    delayMicroseconds(100);

    enableExtensionRE();
    command(static_cast<uint8_t>(ExtendedCommandsRE::functionSelectA));
    data(Settings::ioLevel);

    command(ExtendedCommandsRE::extendedEntryModeSet
            | mExtendedEntryModeSettings);

    /* FIXME: This setting should be moved into an API function executed only if
     * needed. This is because the setting can be hardware defined, so we don't
     * want to override the hard configuration inadvertently. */
    command(ExtendedCommandsRE::functionSelectB);
    data(0x01); // Set ROM A, CGROM 248 => CGRAM = 8
    enableOledCharacterization();
    command(static_cast<uint8_t>(OledCommands::displayClockDividerOscFreq));
    command(Settings::oscillatorFrequency << 4 | Settings::clockDivideRatio);
    command(static_cast<uint8_t>(OledCommands::segPinsHwConfig));
    command(Settings::segPinLeftRightRemap << 5
            | Settings::segPinSequential << 4);
    command(static_cast<uint8_t>(OledCommands::functionSelectC));
    command(Settings::vslSelect << 7 | Settings::gpioSetting);
    command(static_cast<uint8_t>(OledCommands::contrastControl));
    command(Settings::contrast);
    command(static_cast<uint8_t>(OledCommands::phaseLength));
    command(Settings::phase2Length << 4 | Settings::phase1Length);
    command(static_cast<uint8_t>(OledCommands::vcomhDeselectLevel));
    command(Settings::vcomDeselectLevel << 4);
    disableOledCharacterization();
    command(FundamentalCommands::entryModeSet | mEntryModeSettings);
}


// Primary high-level API functions generally compatible with LiquidCrystal

template<typename Interface, typename Settings>
void Us2066<Interface, Settings>::begin(uint8_t columns, uint8_t rows,
                                        uint8_t wideChar)
{
    if (rows > Constants::maxRows) {
        rows = Constants::maxRows;
    }
    mRows = rows;

    if (columns > Constants::maxColumns) {
        columns = Constants::maxColumns;
    }
    mColumns = columns;

    if (mRows > 2) {
        mExtendedFunctionSetSettings |= Bitmasks::threeFourLineDisplay;
    }
    bool rowsIsOdd = static_cast<bool>(mRows & static_cast<uint8_t>(0x01));
    if (!rowsIsOdd) {
        mFunctionSetSettings |= Bitmasks::numRowsEven;
    }

    if (static_cast<bool>(wideChar)) {
        mExtendedFunctionSetSettings |= Bitmasks::sixDotFontWidth;
    }

    command(FundamentalCommands::functionSet | mFunctionSetSettings);
    command(
        static_cast<ExtendedCommandsRE>(ExtendedCommandsRE::extendedFunctionSet
                                        | mExtendedFunctionSetSettings));
    clear();
    home();
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::autoscroll()
{
    mEntryModeSettings |= Bitmasks::autoscroll;
    command(FundamentalCommands::entryModeSet | mEntryModeSettings);
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::noAutoscroll()
{
    mEntryModeSettings &= ~Bitmasks::autoscroll;
    command(FundamentalCommands::entryModeSet | mEntryModeSettings);
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::blink()
{
    mDisplayControlSettings |= Bitmasks::blinkOn;
    command(FundamentalCommands::displayOnOff | mDisplayControlSettings);
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::noBlink()
{
    mDisplayControlSettings &= ~Bitmasks::blinkOn;
    command(FundamentalCommands::displayOnOff | mDisplayControlSettings);
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::clear()
{
    command(static_cast<uint8_t>(FundamentalCommands::clearDisplay));
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::cursor()
{
    mDisplayControlSettings |= Bitmasks::cursorOn;
    command(FundamentalCommands::displayOnOff | mDisplayControlSettings);
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::noCursor()
{
    mDisplayControlSettings &= ~Bitmasks::cursorOn;
    command(FundamentalCommands::displayOnOff | mDisplayControlSettings);
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::display()
{
    mDisplayControlSettings |= Bitmasks::displayOn;
    command(FundamentalCommands::displayOnOff | mDisplayControlSettings);
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::noDisplay()
{
    mDisplayControlSettings &= ~Bitmasks::displayOn;
    command(FundamentalCommands::displayOnOff | mDisplayControlSettings);
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::home()
{
    command(static_cast<uint8_t>(FundamentalCommands::returnHome));
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::leftToRight()
{
    mEntryModeSettings |= Bitmasks::leftToRight;
    command(FundamentalCommands::entryModeSet | mEntryModeSettings);
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::rightToLeft()
{
    mEntryModeSettings &= ~Bitmasks::leftToRight;
    command(FundamentalCommands::entryModeSet | mEntryModeSettings);
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::scrollDisplayLeft()
{
    mCursorDisplayShiftSettings &= ~Bitmasks::shiftRight;
    command(FundamentalCommands::shiftControl
            | (Bitmasks::displayShift | mCursorDisplayShiftSettings));
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::scrollDisplayRight()
{
    mCursorDisplayShiftSettings |= Bitmasks::shiftRight;
    command(FundamentalCommands::shiftControl
            | (Bitmasks::displayShift | mCursorDisplayShiftSettings));
}

template<typename Interface, typename Settings>
void Us2066<Interface, Settings>::setCursor(uint8_t column, uint8_t row)
{
    // Wrap row to correct number of lines
    // Since rows is a power of 2 (2 or 4), use bitwise and instead of modulo
    row &= mRows - 1;
    uint8_t rowOffset = Constants::rowOffset;
    uint8_t address   = column + row * rowOffset;
    command(FundamentalCommands::setDdramAddress | address);
}


// API functions provided by US2066 not available on HD44780 devices

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::doubleHeight()
{
    mFunctionSetSettings |= Bitmasks::doubleHeight;
    command(FundamentalCommands::functionSet | mFunctionSetSettings);
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::noDoubleHeight()
{
    mFunctionSetSettings &= ~Bitmasks::doubleHeight;
    command(FundamentalCommands::functionSet | mFunctionSetSettings);
}

/*! Set the current double height font mode, which enlarges fonts such that they
 * span 2 vertical lines instead of 1. On a 2-line display this setting is
 * meaningless. On a 4-line display there are 4 possible modes: top, middle,
 * bottom and entire (i.e. top and bottom at the same time). POR value is
 * equivalent to top.
 */
template<typename Interface, typename Settings>
void Us2066<Interface, Settings>::doubleHeightMode(DoubleHeightMode mode)
{
    mDoubleHeightSettings &= ~Bitmasks::doubleHeightMode;
    mDoubleHeightSettings |= mode;
    command(ExtendedCommandsRE::doubleHeightModeDisplayShift
            | mDoubleHeightSettings);
}


// Generic API functions, primarily meant for use by higher level API code

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::enableExtensionIS()
{
    mFunctionSetSettings |= Bitmasks::enableExtensionIS;
    command(FundamentalCommands::functionSet | mFunctionSetSettings);
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::disableExtensionIS()
{
    mFunctionSetSettings &= ~Bitmasks::enableExtensionIS;
    command(FundamentalCommands::functionSet | mFunctionSetSettings);
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::enableOledCharacterization()
{
    enableExtensionRE();
    mOledCharacterizationSettings |= Bitmasks::enableOledCharacterization;
    command(ExtendedCommandsRE::oledCharacterization
            | mOledCharacterizationSettings);
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::disableOledCharacterization()
{
    mOledCharacterizationSettings &= ~Bitmasks::enableOledCharacterization;
    command(ExtendedCommandsRE::oledCharacterization
            | mOledCharacterizationSettings);
    disableExtensionRE();
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::enableExtensionRE()
{
    mFunctionSetSettings |= Bitmasks::enableExtensionRE;
    command(FundamentalCommands::functionSet | mFunctionSetSettings);
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::disableExtensionRE()
{
    mFunctionSetSettings &= ~Bitmasks::enableExtensionRE;
    command(FundamentalCommands::functionSet | mFunctionSetSettings);
}

template<typename Interface, typename Settings>
void Us2066<Interface, Settings>::command(ExtendedCommandsIS extendedCommand)
{
    enableExtensionIS();
    command(static_cast<uint8_t>(extendedCommand));
    disableExtensionIS();
}

template<typename Interface, typename Settings>
void Us2066<Interface, Settings>::command(ExtendedCommandsRE extendedCommand)
{
    enableExtensionRE();
    command(static_cast<uint8_t>(extendedCommand));
    disableExtensionRE();
}

template<typename Interface, typename Settings>
inline void
Us2066<Interface, Settings>::command(FundamentalCommands fundamentalCommand)
{
    command(static_cast<uint8_t>(fundamentalCommand));
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::command(uint8_t command)
{
    mInterface.transmit(command, TransmitMode::command);
}

template<typename Interface, typename Settings>
inline void Us2066<Interface, Settings>::data(uint8_t byte)
{
    mInterface.transmit(byte, TransmitMode::data);
}

template<typename Interface, typename Settings>
inline size_t Us2066<Interface, Settings>::write(uint8_t byte)
{
    return mInterface.transmit(byte, TransmitMode::data);
}

} // namespace us2066
