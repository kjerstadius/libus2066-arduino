libus2066-arduino
=================

[![pipeline status](https://gitlab.com/kjerstadius/libus2066-arduino/badges/master/pipeline.svg)](https://gitlab.com/kjerstadius/libus2066-arduino/pipelines)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=kjerstadius_libus2066-arduino&metric=alert_status)](https://sonarcloud.io/dashboard?id=kjerstadius_libus2066-arduino)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=kjerstadius_libus2066-arduino&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=kjerstadius_libus2066-arduino)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=kjerstadius_libus2066-arduino&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=kjerstadius_libus2066-arduino)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=kjerstadius_libus2066-arduino&metric=sqale_index)](https://sonarcloud.io/dashboard?id=kjerstadius_libus2066-arduino)
[![code complexity](https://img.shields.io/endpoint.svg?url=https://kjerstadius.gitlab.io/libus2066-arduino/badges/complexity.average-complexity-ccn.json)](https://kjerstadius.gitlab.io/libus2066-arduino/analysis/complexity.html)
[![kloc](https://img.shields.io/endpoint.svg?url=https://kjerstadius.gitlab.io/libus2066-arduino/badges/complexity.kloc.json)](https://kjerstadius.gitlab.io/libus2066-arduino/analysis/complexity.html)

The [US2066] is a display driver IC for OLED character displays used by several
manufacturers, such as Newhaven Display and Densitron. This library provides
Arduino compatibility for such displays. The primary goals of this project are:

* Provide a familiar API (i.e. [LiquidCrystal]) for US2066-based displays to
  simplify usage and trivialise migration.
* Extend the API to cover features not available on traditional HD44780 based
  displays, such as double font height mode, contrast set, blink and fade out
  modes, etc.
* Decouple the high-level API from the communication interface in order to
  support both serial and parallel operation (*Note: Only 4-bit parallel
  68XX-style communication is supported at present*).

Furthermore, this project aims to make use of "modern" c++ features. As such it
requires a compiler that supports at least c++11.

This library is at present primarily developed and tested using [Teensy 3]
boards, which use ARM [Cortex-M4] MCUs (M0+ in case of the LC). Therefore,
compatibility with AVR-based Arduino devices, or indeed any other non-Teensy 3
device, is unverified. However, as long as `digitalWriteFast()` is available,
the library should work. If it doesn't, please [raise an issue][issues], or
[submit a merge request](CONTRIBUTING.md) if you identified and fixed the problem.

Some design ideas used in this project were "borrowed" from Francois Best's
[Arduino MIDI library]. Thanks for the inspiration!

Build
=====

This library is designed to support building without the Arduino IDE, using the
[Meson build system][Meson]. Currently, this is only supported for Teensy
boards.

Install Meson
-------------

If you don't already have Meson installed, in order to setup the build, first
ensure Python 3.5 or higher is installed. Then run

```console
$ pip install -r requirements.txt
```

from the repository's root directory. **Note**: *It's highly recommended to
separate Python package installations for different projects using virtual
environments. See [venv]*.

Build Firmware Image
--------------------
With Meson installed, setup a build directory:

```console
$ meson my-build-dir --cross-file teensy3-gcc.txt
```

The default behavior is to build binaries for the Teensy 3.2 board, but this
setting can be changed using `meson configure`, or by passing the setting on the
command line as a compiler flag when creating the build directory in the first
place. For example,

```console
$ meson configure my-build-dir -Dlibteensycore:variant=3.6
```

will change the target Teensy board to 3.6. To list all available settings,
remove the `-Dlibteensycore:variant=3.6` of the above command.

In order to build the library and some example firmware files, run

```console
$ cd my-build-dir
[my-build-dir]$ ninja
```

Some convenient top level targets are defined for the provided example code
(found in the `examples` folder). E.g., in order to list the size of the
`hello_world` firmware, run

```console
[my-build-dir]$ ninja hello_world.size
```

If the [Teensy Loader CLI][loader-cli] was found in the `PATH` when the build
directory was setup there's also a target that can be used for building and
flashing the binary directly to a connected board. Again, using `hello_world`
as an example:

```console
[my-build-dir]$ ninja hello_world.flash
```

will compile and upload the firmware to a connected board.


Contributing
============

See [Contributing](CONTRIBUTING.md).

<!-- Hyperlinks -->

[Arduino MIDI library]: https://github.com/FortySevenEffects/arduino_midi_library
[Cortex-M4]: https://en.wikipedia.org/wiki/ARM_Cortex-M#Cortex-M4
[issues]: https://gitlab.com/kjerstadius/libus2066-arduino/issues
[LiquidCrystal]: https://www.arduino.cc/en/Reference/LiquidCrystal
[loader-cli]: https://github.com/PaulStoffregen/teensy_loader_cli
[Meson]: https://mesonbuild.com
[Teensy 3]: https://www.pjrc.com/teensy/
[US2066]: https://www.displayfuture.com/Display/datasheet/controller/US2066.pdf
[venv]: https://docs.python.org/3/library/venv.html
